FROM quay.io/wildfly/wildfly:30.0.1.Final-jdk17

USER root

ADD files /files

RUN keytool -noprompt \
    -import -trustcacerts -cacerts -alias statastic \
    -storepass changeit \
    -file /files/statastic-rootCA.der \
    && mkdir /data \
    && mv /files/run.sh /opt/jboss/wildfly/bin/run.sh \
    && chown jboss:jboss /opt/jboss/wildfly/bin/run.sh \
    && chown jboss:jboss /data

USER jboss

RUN /opt/jboss/wildfly/bin/jboss-cli.sh --file=/files/notes-ds.cli

# for drop-in deployments
VOLUME /opt/jboss/wildfly/standalone/deployments/

# for h2 databases
VOLUME /data

ENV ADMIN_USER=""
ENV ADMIN_PASSWORD=""
ENV NOTES_DB_USER=sa
ENV NOTES_DB_PASSWORD=sa

CMD ["/opt/jboss/wildfly/bin/run.sh"]
