#!/bin/bash

# create an admin user if env-variable is set
if [ -n "$ADMIN_USER" ]; then
  echo "creating admin user ${ADMIN_USER}"
  /opt/jboss/wildfly/bin/add-user.sh ${ADMIN_USER} ${ADMIN_PASSWORD} --silent
else
  echo "no admin user supplied"
fi

/opt/jboss/wildfly/bin/standalone.sh -b 0.0.0.0 -bmanagement 0.0.0.0